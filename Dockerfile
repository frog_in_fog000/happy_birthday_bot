# build
FROM golang:1.22.1-alpine AS builder

WORKDIR /app

RUN apk --no-cache add bash git make

COPY ["go.mod", "go.sum", "./"]

RUN go mod download

COPY . .

RUN  CGO_ENABLED=0 go build -o ./bin/app cmd/*.go

# run
FROM alpine as runner

COPY --from=builder /app/bin/app /

CMD ["/app"]
