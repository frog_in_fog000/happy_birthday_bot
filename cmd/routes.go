package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/handlers"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/middleware"
)

func InitRoutes(h handlers.HBHandlers, m *middleware.Middleware) http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/signup", h.SignUp)
	router.HandleFunc("/signin", h.SignIn)

	api := router.PathPrefix("/api").Subrouter()
	api.Use(m.AuthRequired)

	api.HandleFunc("/logout", h.Logout)
	api.HandleFunc("/employees", h.GetEmployees)
	api.HandleFunc("/employee", h.GetEmployeesByBirthday)
	api.HandleFunc("/subscribe", h.Subscribe)
	api.HandleFunc("/unsubscribe", h.Unsubscribe)

	return router
}
