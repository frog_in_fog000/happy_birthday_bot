package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/robfig/cron/v3"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/config"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/handlers"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/middleware"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/service"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage/postgres"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage/redis_storage"
	tgbot "gitlab.com/frog_in_fog000/happy_birthday_bot/internal/tg_bot"
)

type Server struct {
	server *http.Server
}

func main() {
	log.Println(time.Now())
	// config init
	var cfg config.Config
	if err := cleanenv.ReadEnv(&cfg); err != nil {
		log.Fatalf("error initializing config: %v", err)
	}

	// default context init
	ctx := context.Background()

	// init bot
	bot, err := tgbotapi.NewBotAPI(cfg.BotApiKey)
	if err != nil {
		log.Printf("error initializing tg bot: %v", err)
		return
	}

	// run bot
	go runBot(bot, cfg)

	// db setup
	postgresStorage, db := postgres.NewPostgresStorage(ctx, cfg.DSN)
	defer db.Close()

	// redis client init
	redisClient, err := redis_storage.NewRedisConnection(ctx, &cfg)
	if err != nil {
		log.Fatal(err)
		return
	}

	// service init
	hbService := service.NewHBService(ctx, cfg, postgresStorage, redisClient, bot)

	// handlers init
	hbHandlers := handlers.NewHBHandlers(ctx, hbService)

	// middleware init
	hbMiddleware := middleware.NewHBMiddleware(ctx, cfg, redisClient)

	// init routes
	hbRoutes := InitRoutes(hbHandlers, hbMiddleware)

	// launch http server
	server := new(Server)

	// init reminder
	go startNotificationService(hbService)

	go func() {
		if err := server.run(cfg.Port, hbRoutes); err != nil {
			log.Fatalf("error occured running http server: %v", err)
		}
	}()

	log.Printf("Application started on port: %s", cfg.Port)
	gracefulShutdown(*server)
	log.Println("Application stopped")
}

func startNotificationService(s service.HBService) {
	c := cron.New()
	c.AddFunc("@daily", s.SendBirthdayNotifications)
	c.Start()
}

func runBot(bot *tgbotapi.BotAPI, cfg config.Config) error {
	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	botHandlers := tgbot.NewBotHandlers(bot, cfg)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		botHandlers.HandleBotUpdates(update.Message)
	}

	return nil
}

func (s *Server) run(port string, handler http.Handler) error {
	s.server = &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      handler,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return s.server.ListenAndServe()
}

func (s *Server) shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}

func gracefulShutdown(server Server) {
	q := make(chan os.Signal, 1)
	signal.Notify(q, syscall.SIGTERM, syscall.SIGINT)
	<-q

	if err := server.shutdown(context.Background()); err != nil {
		log.Printf("error occurred on server shutting down: %v", err)
	}
}
