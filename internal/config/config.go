package config

import "time"

type Config struct {
	Port      string `env:"PORT"`
	Host      string `env:"HOST"`
	BotApiKey string `env:"BOT_API_KEY"`
	RedisURL  string `env:"REDIS_URL"`

	AccessTokenExpiresIn  time.Duration `env:"ACCESS_TOKEN_EXPIRED_IN"`
	AccessTokenPrivateKey string        `env:"ACCESS_TOKEN_PRIVATE_KEY"`
	AccessTokenPublicKey  string        `env:"ACCESS_TOKEN_PUBLIC_KEY"`
	DSN                   string        `env:"DSN"`
}
