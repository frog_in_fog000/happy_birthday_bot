package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
)

func (h *hbHandlers) Unsubscribe(w http.ResponseWriter, r *http.Request) {
	var unsubFromRequest dto.SubscribeUserDTO

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = json.Unmarshal(reqBody, &unsubFromRequest); err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	user, err := h.hbService.GetUserByUsername(unsubFromRequest.UserName)
	if err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	employee, err := h.hbService.GetEmployeeByEmployeeName(unsubFromRequest.EmployeeName)
	if err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = h.hbService.Unsubscribe(user.ID, employee.ID); err != nil {
		log.Printf("error unsubscribing: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Вы успешно отписались от уведомлений о днях рождения пользователя: %s", employee.Name)))
}
