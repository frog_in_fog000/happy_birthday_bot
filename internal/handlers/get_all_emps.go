package handlers

import (
	"encoding/json"
	"log"
	"net/http"
)

func (h *hbHandlers) GetEmployees(w http.ResponseWriter, r *http.Request) {
	employees, err := h.hbService.GetEmployees()
	if err != nil {
		log.Printf("error getting employees: %v", err)
		http.Error(w, "something went wrong...", http.StatusInternalServerError)
		return
	}
	if len(employees) == 0 {
		w.Write([]byte("Работники не найдены..."))
		return
	}

	jsonEmployees, err := json.Marshal(employees)
	if err != nil {
		log.Printf("error marshalling response: %v", err)
		http.Error(w, "something went wrong...", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonEmployees)
}
