package handlers

import (
	"context"
	"net/http"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/service"
)

type HBHandlers interface {
	SignUp(w http.ResponseWriter, r *http.Request)
	SignIn(w http.ResponseWriter, r *http.Request)
	Logout(w http.ResponseWriter, r *http.Request)
	GetEmployees(w http.ResponseWriter, r *http.Request)
	GetEmployeesByBirthday(w http.ResponseWriter, r *http.Request)
	Subscribe(w http.ResponseWriter, r *http.Request)
	Unsubscribe(w http.ResponseWriter, r *http.Request)
}

type hbHandlers struct {
	ctx       context.Context
	hbService service.HBService
}

func NewHBHandlers(ctx context.Context, hbService service.HBService) HBHandlers {
	return &hbHandlers{ctx: ctx, hbService: hbService}
}
