package handlers

import (
	"log"
	"net/http"
)

func (h *hbHandlers) Logout(w http.ResponseWriter, r *http.Request) {
	username := r.Header.Get("X-USERNAME")
	if len(username) == 0 {
		log.Printf("error getting username header")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так. Пожалуйста, повторите попытку позже..."))
		return
	}

	if err := h.hbService.Logout(username); err != nil {
		log.Printf("error logging out: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так. Пожалуйста, повторите попытку позже..."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Вы успешно вышли из свего аккаунта. Чтобы получить доступ к функциям бота, пожалуйста, авторизуйтесь!"))
}
