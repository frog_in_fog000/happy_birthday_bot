package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/service"
)

func (h *hbHandlers) Subscribe(w http.ResponseWriter, r *http.Request) {
	var subFromRequest dto.SubscribeUserDTO

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading request body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = json.Unmarshal(reqBody, &subFromRequest); err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	user, err := h.hbService.GetUserByUsername(subFromRequest.UserName)
	if err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	employee, err := h.hbService.GetEmployeeByEmployeeName(subFromRequest.EmployeeName)
	if err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = h.hbService.Subscribe(user.ID, employee.ID); err != nil {
		if errors.Is(err, service.ErrSubscriptionAlreadyExists) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Похоже, вы уже подписаны на уведомления этого пользователя"))
			return
		}
		log.Printf("error subscribing: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Вы успешно подписались на уведомления о днях рождения пользователя: %s", employee.Name)))
}
