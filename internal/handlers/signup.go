package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/service"
)

func (h *hbHandlers) SignUp(w http.ResponseWriter, r *http.Request) {
	var user dto.SignupUserDTO

	userBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading request body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = json.Unmarshal(userBody, &user); err != nil {
		log.Printf("error unmarshalling request body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	if err = h.hbService.SignUp(&user); err != nil {
		if errors.Is(err, service.ErrUserAlreadyExists) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Пользователь с таким ником уже зарегистрирован"))
			return
		}
		log.Printf("error signing up: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Что-то пошло не так, повторите попытку позже..."))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("Пользователь %s успешно зарегистрирован. Теперь вы можете авторизоваться", user.UserName)))
}
