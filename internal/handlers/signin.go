package handlers

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/service"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (h *hbHandlers) SignIn(w http.ResponseWriter, r *http.Request) {
	var user dto.SigninUserDTO

	userBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading request body: %v", err)
		http.Error(w, "Что-то пошло не так, повторите попытку позже...", http.StatusInternalServerError)
		return
	}

	if err = json.Unmarshal(userBody, &user); err != nil {
		log.Printf("error unmarshalling user body: %v", err)
		http.Error(w, "Что-то пошло не так, повторите попытку позже...", http.StatusInternalServerError)
		return
	}

	if err = h.hbService.SignIn(&user); err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Пользователь не найден. Пожалуйста, пройдите регистрацию"))
		} else if errors.Is(err, service.ErrInvalidCredentials) {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Похоже, вы ввели неверный пароль. Пожалуйста, попробуйте снова."))
		}
		log.Printf("Что-то пошло не так, повторите попытку позже..: %v", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Вы авторизованы. Теперь Вам доступны все функции!"))
}
