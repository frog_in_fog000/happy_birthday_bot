package models

import "time"

type Employee struct {
	ID       string    `json:"id"`
	Name     string    `json:"name"`
	Birthday time.Time `json:"birthday"`
	Email    string    `json:"email"`
}
