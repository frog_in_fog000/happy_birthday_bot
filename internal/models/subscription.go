package models

type Subscription struct {
	ID         string `json:"id"`
	UserID     string `json:"user_id"`
	EmployeeID string `json:"employee_id"`
}
