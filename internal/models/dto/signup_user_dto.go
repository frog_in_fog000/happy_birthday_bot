package dto

type SignupUserDTO struct {
	UserName     string `json:"username"`
	PasswordHash string `json:"password_hash"`
	ChatID       int64  `json:"chat_id"`
}
