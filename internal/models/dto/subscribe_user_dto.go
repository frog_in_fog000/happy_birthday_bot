package dto

type SubscribeUserDTO struct {
	UserName     string `json:"username"`
	EmployeeName string `json:"employee_name"`
}
