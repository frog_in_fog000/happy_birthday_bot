package dto

type SigninUserDTO struct {
	UserName     string `json:"username"`
	PasswordHash string `json:"password_hash"`
	Email        string `json:"email"`
}
