package models

type BirthdayEvent struct {
	ChatID       int64  `json:"chat_id"`
	EmployeeName string `json:"employee_name"`
}
