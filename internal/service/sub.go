package service

import (
	"log"

	"github.com/google/uuid"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (s *hbService) Subscribe(userID string, employeeID string) error {
	var sub models.Subscription
	sub.ID = uuid.NewString()
	sub.UserID = userID
	sub.EmployeeID = employeeID

	exists, err := s.storage.CheckSubscriptionExists(s.ctx, &sub)
	if err != nil {
		log.Printf("error checking sub exists: %v", err)
		return err
	}

	if exists {
		return ErrSubscriptionAlreadyExists
	} else {
		if err := s.storage.CreateSubscribtion(s.ctx, &sub); err != nil {
			log.Printf("error creating subscribtion: %v", err)
			return err
		}
	}
	return nil
}
