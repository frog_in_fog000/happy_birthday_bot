package service

import (
	"context"
	"errors"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/config"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

var (
	ErrInvalidCredentials        = errors.New("invalid credentials")
	ErrUserAlreadyExists         = errors.New("user already exists")
	ErrSubscriptionAlreadyExists = errors.New("subscription already exists")
)

type HBService interface {
	SignUp(user *dto.SignupUserDTO) error
	SignIn(user *dto.SigninUserDTO) error
	Logout(userID string) error
	GetEmployees() ([]*models.Employee, error)
	GetEmployeesByBirthday(day time.Time) ([]*models.Employee, error)
	GetUserByUsername(username string) (*models.User, error)
	GetEmployeeByEmployeeName(employeeName string) (*models.Employee, error)
	Subscribe(userID string, employeeID string) error
	Unsubscribe(userID string, employeeID string) error
	SendBirthdayNotifications()
}

type hbService struct {
	ctx         context.Context
	cfg         config.Config
	storage     storage.HBStorage
	redisClient *redis.Client
	bot         *tgbotapi.BotAPI
}

func NewHBService(ctx context.Context, cfg config.Config, hbStorage storage.HBStorage, redisClient *redis.Client, bot *tgbotapi.BotAPI) HBService {
	return &hbService{ctx: ctx, cfg: cfg, storage: hbStorage, redisClient: redisClient, bot: bot}
}
