package service

import (
	"errors"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (s *hbService) GetEmployeeByEmployeeName(employeeName string) (*models.Employee, error) {
	employee, err := s.storage.GetEmployeeByEmployeeName(s.ctx, employeeName)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			return &models.Employee{}, storage.ErrUserNotFound
		}

		return nil, err
	}

	return employee, nil
}
