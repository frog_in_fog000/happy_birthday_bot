package service

import "log"

func (s *hbService) Logout(username string) error {
	if err := s.redisClient.Del(s.ctx, username).Err(); err != nil {
		log.Printf("error removing token from redis: %v", err)
		return err
	}

	return nil
}
