package service

import (
	"errors"
	"log"
	"time"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/jwt_auth"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
	"golang.org/x/crypto/bcrypt"
)

func (s *hbService) SignIn(user *dto.SigninUserDTO) error {
	userFromDB, err := s.storage.GetUserByUsername(s.ctx, user.UserName)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			return storage.ErrUserNotFound
		}
		log.Printf("error getting user by username: %v", err)
		return err
	}

	if err = bcrypt.CompareHashAndPassword([]byte(userFromDB.PasswordHash), []byte(user.PasswordHash)); err != nil {
		log.Printf("error comparing password hash: %v", err)
		return ErrInvalidCredentials
	}

	accessTokenDetails, err := jwt_auth.CreateToken(user.UserName, s.cfg.AccessTokenExpiresIn, s.cfg.AccessTokenPrivateKey)
	if err != nil {
		log.Printf("error creating access token: %v", err)
		return err
	}

	if err = s.redisClient.Set(s.ctx, user.UserName, *accessTokenDetails.Token, time.Unix(*accessTokenDetails.ExpiresIn, 0).Sub(time.Now())).Err(); err != nil {
		log.Printf("error setting access token to Redis: %v", err)
		return err
	}

	return nil
}
