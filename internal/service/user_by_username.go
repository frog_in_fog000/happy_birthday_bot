package service

import (
	"errors"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (s *hbService) GetUserByUsername(username string) (*models.User, error) {
	user, err := s.storage.GetUserByUsername(s.ctx, username)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			return &models.User{}, storage.ErrUserNotFound
		}

		return nil, err
	}

	return user, nil
}
