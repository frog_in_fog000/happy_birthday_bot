package service

import (
	"errors"
	"log"

	"github.com/google/uuid"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
	"golang.org/x/crypto/bcrypt"
)

func (s *hbService) SignUp(inputData *dto.SignupUserDTO) error {
	var user models.User

	_, err := s.storage.GetUserByUsername(s.ctx, inputData.UserName)
	if !errors.Is(err, storage.ErrUserNotFound) {
		log.Println("user with such username already exists")
		return ErrUserAlreadyExists
	}
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(inputData.PasswordHash), bcrypt.DefaultCost)
	if err != nil {
		log.Printf("error generating password hash: %v", err)
		return err
	}

	user.ID = uuid.NewString()
	user.Username = inputData.UserName
	user.PasswordHash = string(passwordHash)
	user.ChatID = inputData.ChatID

	if err = s.storage.CreateUser(s.ctx, &user); err != nil {
		log.Printf("error inserting user to the database: %v", err)
		return err
	}

	return nil
}
