package service

import "gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"

func (s *hbService) GetEmployees() ([]*models.Employee, error) {
	employees, err := s.storage.GetEmployees(s.ctx)
	if err != nil {
		return nil, err
	}

	return employees, nil
}
