package service

import (
	"errors"
	"fmt"
	"log"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (s *hbService) SendBirthdayNotifications() {
	today := time.Now()

	log.Print("Inside notification service")

	events, err := s.storage.GetBirthdayEvents(s.ctx, today)
	if err != nil {
		if errors.Is(err, storage.ErrBDEventNotFound) {
			log.Printf("no birth days found today: %v", today.Format("2006-01-02"))
		}
		log.Printf("error getting birthday events: %v", err)
		return
	}

	log.Println("Events from service: ", events)

	for _, event := range events {
		msg := tgbotapi.NewMessage(event.ChatID, fmt.Sprintf("Сегодня у %s день рождения! Поздравьте его скорее!", event.EmployeeName))
		s.bot.Send(msg)
	}
}
