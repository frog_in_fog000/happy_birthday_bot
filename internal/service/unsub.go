package service

import (
	"log"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (s *hbService) Unsubscribe(userID string, employeeID string) error {
	var unsub models.Subscription
	unsub.UserID = userID
	unsub.EmployeeID = employeeID

	if err := s.storage.DeleteSubscribtion(s.ctx, &unsub); err != nil {
		log.Printf("error deleting subscription: %v", err)
		return err
	}

	return nil
}
