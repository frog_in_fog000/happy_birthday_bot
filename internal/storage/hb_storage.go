package storage

import (
	"context"
	"errors"
	"time"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

var (
	ErrUserNotFound    = errors.New("user not found")
	ErrBDEventNotFound = errors.New("birthday events not found today")
)

type HBStorage interface {
	CreateUser(ctx context.Context, user *models.User) error
	GetUserByUsername(ctx context.Context, userID string) (*models.User, error)
	GetEmployeeByEmployeeName(ctx context.Context, employeeName string) (*models.Employee, error)
	DeleteUser(ctx context.Context, userID string) error
	GetEmployees(ctx context.Context) ([]*models.Employee, error)
	GetBirthdayEvents(ctx context.Context, day time.Time) ([]*models.BirthdayEvent, error)
	CreateSubscribtion(ctx context.Context, subscription *models.Subscription) error
	DeleteSubscribtion(ctx context.Context, subscription *models.Subscription) error
	CheckSubscriptionExists(ctx context.Context, subscription *models.Subscription) (bool, error)
}
