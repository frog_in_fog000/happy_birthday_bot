package redis_storage

import (
	"context"
	"log"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/config"
)

func NewRedisConnection(ctx context.Context, cfg *config.Config) (*redis.Client, error) {
	redisCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.RedisURL,
		Password: "",
		DB:       0,
	})

	if _, err := redisClient.Ping(redisCtx).Result(); err != nil {
		log.Fatalf("redis connection error: %v", err)
		return nil, err
	}

	log.Println("redis client connected successfully")
	return redisClient, nil
}
