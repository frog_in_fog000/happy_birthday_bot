package postgres

import (
	"context"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (p *postgresStorage) DeleteSubscribtion(ctx context.Context, subscription *models.Subscription) error {
	_, err := p.db.Exec(ctx,
		`DELETE FROM subscriptions WHERE user_id = $1 AND employee_id = $2`,
		subscription.UserID, subscription.EmployeeID)
	if err != nil {
		return err
	}

	return nil
}
