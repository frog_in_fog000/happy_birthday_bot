package postgres

import (
	"context"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (p *postgresStorage) CreateUser(ctx context.Context, user *models.User) error {
	_, err := p.db.Exec(ctx,
		`INSERT INTO users (id, username, password, chat_id) VALUES ($1, $2, $3, $4)`,
		user.ID, user.Username, user.PasswordHash, user.ChatID)
	if err != nil {
		return err
	}

	return nil
}
