package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (p *postgresStorage) GetEmployeeByEmployeeName(ctx context.Context, employee_name string) (*models.Employee, error) {
	var employee models.Employee
	err := p.db.QueryRow(ctx,
		`SELECT id, name, birthday, email FROM employees WHERE name = $1`,
		employee_name).Scan(&employee.ID, &employee.Name, &employee.Birthday, &employee.Email)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return &models.Employee{}, storage.ErrUserNotFound
		}
		return nil, err
	}

	return &employee, nil
}
