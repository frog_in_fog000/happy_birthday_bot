package postgres

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (p *postgresStorage) GetEmployees(ctx context.Context) ([]*models.Employee, error) {
	ctxWithTimeout, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	rows, err := p.db.Query(ctxWithTimeout,
		`SELECT id, name, birthday, email FROM employees ORDER BY name`)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return []*models.Employee{}, nil
		}
		return nil, err
	}
	defer rows.Close()

	employees := make([]*models.Employee, 0)
	for rows.Next() {
		var employee models.Employee
		if err = rows.Scan(&employee.ID, &employee.Name, &employee.Birthday, &employee.Email); err != nil {
			return nil, err
		}

		employees = append(employees, &employee)
	}

	return employees, nil
}
