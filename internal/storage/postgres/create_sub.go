package postgres

import (
	"context"

	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (p *postgresStorage) CreateSubscribtion(ctx context.Context, subscription *models.Subscription) error {
	_, err := p.db.Exec(ctx,
		`INSERT INTO subscriptions (id, user_id, employee_id) VALUES ($1, $2, $3)`,
		subscription.ID, subscription.UserID, subscription.EmployeeID)
	if err != nil {
		return err
	}

	return nil
}
