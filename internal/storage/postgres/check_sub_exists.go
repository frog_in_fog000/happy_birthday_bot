package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (p *postgresStorage) CheckSubscriptionExists(ctx context.Context, subscription *models.Subscription) (bool, error) {
	var sub models.Subscription
	err := p.db.QueryRow(ctx,
		`SELECT id, user_id, employee_id FROM subscriptions WHERE user_id = $1 AND employee_id = $2`,
		subscription.UserID, subscription.EmployeeID).Scan(&sub.ID, &sub.UserID, &sub.EmployeeID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return false, nil
		}
		return true, err
	}

	return true, nil
}
