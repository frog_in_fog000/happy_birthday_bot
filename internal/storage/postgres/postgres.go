package postgres

import (
	"context"
	"log"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

type postgresStorage struct {
	db *pgxpool.Pool
}

func NewPostgresStorage(ctx context.Context, dsn string) (storage.HBStorage, *pgxpool.Pool) {
	db, err := pgxpool.New(ctx, dsn)
	if err != nil {
		log.Fatalf("error connecting to postgres: %v", err)
	}
	return &postgresStorage{db: db}, db
}
