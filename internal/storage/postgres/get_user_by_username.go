package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (p *postgresStorage) GetUserByUsername(ctx context.Context, username string) (*models.User, error) {
	var user models.User
	err := p.db.QueryRow(ctx,
		`SELECT id, username, password, chat_id FROM users WHERE username = $1`,
		username).Scan(&user.ID, &user.Username, &user.PasswordHash, &user.ChatID)
	if err != nil {
		// handle: no rows in result set
		if errors.Is(err, pgx.ErrNoRows) {
			return &models.User{}, storage.ErrUserNotFound
		}
		return nil, err
	}

	return &user, nil
}
