package postgres

import "context"

func (p *postgresStorage) DeleteUser(ctx context.Context, userID string) error {
	_, err := p.db.Exec(ctx,
		`DELETE FROM users WHERE id = $1`,
		userID)
	if err != nil {
		return err
	}

	return nil
}
