package postgres

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/jackc/pgx/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/storage"
)

func (p *postgresStorage) GetBirthdayEvents(ctx context.Context, day time.Time) ([]*models.BirthdayEvent, error) {
	log.Print("Inside notification storage")
	today := day.Format("01-02")
	rows, err := p.db.Query(ctx,
		`SELECT u.chat_id, e.name
								FROM subscriptions s
									JOIN employees e ON s.employee_id = e.id
									JOIN users u ON u.id = s.user_id
										WHERE to_char(e.birthday, 'MM-DD') = $1`,
		today)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return []*models.BirthdayEvent{}, storage.ErrBDEventNotFound
		}

		return nil, err
	}
	defer rows.Close()

	birthdayEvents := make([]*models.BirthdayEvent, 0)

	for rows.Next() {
		var event models.BirthdayEvent

		if err = rows.Scan(&event.ChatID, &event.EmployeeName); err != nil {
			return nil, err
		}

		birthdayEvents = append(birthdayEvents, &event)
	}

	log.Println("Events from storage: ", birthdayEvents)

	return birthdayEvents, nil
}
