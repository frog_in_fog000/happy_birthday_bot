package tgbot

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (h *botHandlers) GetEmployees(message *tgbotapi.Message) {
	requestURL := fmt.Sprintf("http://%s:%s/api/employees", h.cfg.Host, h.cfg.Port)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		log.Printf("error creating request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("X-USERNAME", message.From.UserName)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("error sending request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("error reading response body: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	var employees []models.Employee
	if err = json.Unmarshal(respBody, &employees); err != nil {
		log.Printf("error unmarshalling employees: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Попробуйте авторизоваться повторно")
		h.bot.Send(reply)
		return
	}

	var builder strings.Builder

	// Добавляем заголовок
	builder.WriteString("Список работников:\n")
	builder.WriteString("=================\n")

	// Добавляем данные о каждом человеке
	for _, employee := range employees {
		builder.WriteString(fmt.Sprintf("Имя: %s\n", employee.Name))
		builder.WriteString(fmt.Sprintf("Дата рождения: %s\n", employee.Birthday.Format("02-01-2006")))
		builder.WriteString(fmt.Sprintf("Email: %s\n", employee.Email))
		builder.WriteString("-----------------\n")
	}

	reply := tgbotapi.NewMessage(message.Chat.ID, builder.String())
	h.bot.Send(reply)
}
