package tgbot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models/dto"
)

func (h *botHandlers) Unsubscribe(message *tgbotapi.Message) {
	parts := strings.Split(message.Text, " ")

	if len(parts) != 3 {
		reply := tgbotapi.NewMessage(message.Chat.ID, "Неверный формат данных. Пожалуйста, введите команду в формате: /unsubscribe <firstname> <lastname>")
		h.bot.Send(reply)
		return
	}

	firstName := parts[1]
	lastName := parts[2]

	employeeName := strings.ToLower(firstName + " " + lastName)

	var unsubscribeReqBody dto.SubscribeUserDTO
	unsubscribeReqBody.UserName = message.From.UserName
	unsubscribeReqBody.EmployeeName = employeeName

	jsonSubscribeReqBody, err := json.Marshal(unsubscribeReqBody)
	if err != nil {
		log.Printf("error marshalling subscribe input json: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	requestURL := fmt.Sprintf("http://%s:%s/api/unsubscribe", h.cfg.Host, h.cfg.Port)
	req, err := http.NewRequest(http.MethodPost, requestURL, bytes.NewBuffer(jsonSubscribeReqBody))
	if err != nil {
		log.Printf("error creating request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("X-USERNAME", message.From.UserName)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("error sending request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("error reading response body: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	reply := tgbotapi.NewMessage(message.Chat.ID, string(respBody))
	h.bot.Send(reply)
}
