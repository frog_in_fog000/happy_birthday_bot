package session

import (
	"sync"
	"time"
)

type UserSession struct {
	Username string
	Timer    *time.Timer
}

var authorizedUsers = struct {
	sync.RWMutex
	listOfUsers map[string]*UserSession
}{
	listOfUsers: make(map[string]*UserSession),
}

func addUserToSession(username string, duration time.Duration) {
	authorizedUsers.Lock()
	defer authorizedUsers.Unlock()

	if session, exists := authorizedUsers.listOfUsers[username]; exists {
		session.Timer.Stop()
	}

	timer := time.AfterFunc(duration, func() {
		removeUserFromSession(username)
	})

	authorizedUsers.listOfUsers[username] = &UserSession{
		Username: username,
		Timer:    timer,
	}
}

func removeUserFromSession(username string) {
	authorizedUsers.Lock()
	defer authorizedUsers.Unlock()

	if session, exists := authorizedUsers.listOfUsers[username]; exists {
		session.Timer.Stop() // Остановить таймер
		delete(authorizedUsers.listOfUsers, username)
	}
}

func isUserAuthorized(username string) bool {
	authorizedUsers.RLock()
	defer authorizedUsers.RUnlock()

	_, exists := authorizedUsers.listOfUsers[username]
	return exists
}
