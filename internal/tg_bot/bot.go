package tgbot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/config"
)

type botHandlers struct {
	cfg config.Config
	bot *tgbotapi.BotAPI
}

func NewBotHandlers(bot *tgbotapi.BotAPI, cfg config.Config) *botHandlers {
	return &botHandlers{bot: bot, cfg: cfg}
}

func (h *botHandlers) HandleBotUpdates(message *tgbotapi.Message) {
	username := message.From.UserName
	chatID := message.Chat.ID

	switch message.Command() {
	case "start":
		h.SendWelcomeMessage(chatID, username)
	case "signup":
		h.SignUp(message)
	case "signin":
		h.SignIn(message)
	case "employees":
		h.GetEmployees(message)
	case "logout":
		h.Logout(message)
	case "subscribe":
		h.Subscribe(message)
	case "unsubscribe":
		h.Unsubscribe(message)
	case "help":
		msg := tgbotapi.NewMessage(chatID, `/signup <password>: команда для регистрации;\n
											/signin <password>: команда для авторизации;
											/logout: команда для выхода из аккаунта;
											/employees: команда для получения списка работников (только АВТОРИЗОВАННЫМ пользователям);
											/subscribe <firstname> <lastname>: команда для подписки на уведомления о днях рождания определенного работника;
											/unsubscribe <firstname> <lastname>: команда для отписки от уведомлений о днях рождания определенного работника
											/help: команда для получения подсказок по пользованию ботом`)
		h.bot.Send(msg)
	default:
		msg := tgbotapi.NewMessage(chatID, "Команда не распознана. Пожалуйста, используйте /help для получения подсказок")
		h.bot.Send(msg)
	}
}
