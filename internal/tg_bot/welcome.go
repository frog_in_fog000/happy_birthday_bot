package tgbot

import (
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func (h *botHandlers) SendWelcomeMessage(chatID int64, username string) {
	msg := tgbotapi.NewMessage(chatID, fmt.Sprintf("Добро пожаловать, %s. Используйте команду /help, чтобы получить советы и инструкции по пользованию данным ботом", username))
	h.bot.Send(msg)
}
