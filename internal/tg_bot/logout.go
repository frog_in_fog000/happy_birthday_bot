package tgbot

import (
	"fmt"
	"io"
	"log"
	"net/http"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func (h *botHandlers) Logout(message *tgbotapi.Message) {
	requestURL := fmt.Sprintf("http://%s:%s/api/logout", h.cfg.Host, h.cfg.Port)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		log.Printf("error creating request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}
	req.Header.Set("X-USERNAME", message.From.UserName)

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("error sending request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("error reading response body: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	reply := tgbotapi.NewMessage(message.Chat.ID, string(respBody))
	h.bot.Send(reply)
}
