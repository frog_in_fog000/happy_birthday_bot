package tgbot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/models"
)

func (h *botHandlers) SignIn(message *tgbotapi.Message) {
	parts := strings.Split(message.Text, " ")

	if len(parts) != 2 {
		reply := tgbotapi.NewMessage(message.Chat.ID, "Неверный формат данных. Пожалуйста, введите команду в формате: /signin password")
		h.bot.Send(reply)
		return
	}

	password := parts[1]

	var user models.User
	user.Username = message.From.UserName
	user.PasswordHash = password

	userJson, err := json.Marshal(user)
	if err != nil {
		log.Printf("error marshalling user json: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	requestURL := fmt.Sprintf("http://%s:%s/signin", h.cfg.Host, h.cfg.Port)
	req, err := http.NewRequest(http.MethodPost, requestURL, bytes.NewBuffer(userJson))
	if err != nil {
		log.Printf("error creating request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Printf("error sending request: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("error reading response body: %v", err)
		reply := tgbotapi.NewMessage(message.Chat.ID, "Внутренняя ошибка. Пожалуйста, повторите позже...")
		h.bot.Send(reply)
		return
	}

	reply := tgbotapi.NewMessage(message.Chat.ID, string(respBody))
	h.bot.Send(reply)
}
