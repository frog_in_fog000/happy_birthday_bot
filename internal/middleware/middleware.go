package middleware

import (
	"context"
	"log"
	"net/http"

	"github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/config"
	"gitlab.com/frog_in_fog000/happy_birthday_bot/internal/jwt_auth"
)

type Middleware struct {
	ctx         context.Context
	cfg         config.Config
	redisClient *redis.Client
}

func NewHBMiddleware(ctx context.Context, cfg config.Config, redisClient *redis.Client) *Middleware {
	return &Middleware{ctx: ctx, cfg: cfg, redisClient: redisClient}
}

func (m *Middleware) AuthRequired(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userName := r.Header.Get("X-USERNAME")
		if len(userName) == 0 {
			log.Printf("error getting username header")
			return
		}
		accessToken, err := m.redisClient.Get(m.ctx, userName).Result()
		if err != nil {
			log.Printf("error receining token from redis: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Что-то пошло не так, попробуйте авторизоваться повторно"))
			return
		}

		_, err = jwt_auth.ValidateToken(accessToken, m.cfg.AccessTokenPublicKey)
		if err != nil {
			log.Printf("error validating access token: %v", err)
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Что-то пошло не так, попробуйте авторизоваться повторно"))
			return
		}

		w.WriteHeader(http.StatusAccepted)
		w.Header().Set("X-USERNAME", userName)
		next.ServeHTTP(w, r)
	})
}
